package com.thoughtworks.korprulu.template;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.thoughtworks.korprulu.template")
@EntityScan({
    "com.thoughtworks.korprulu.template.infrastructure.persistence.po"
})
public class KorpruluApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(KorpruluApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }
}
